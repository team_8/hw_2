#include <jni.h>
#include <string>
#include <vector>
#include <cstring>
#include <math.h>
extern "C" {
    #include "aes-lib.c"
}

int priority(char);
double operate(double, double,char);
double operate(double, std::string);
double squ(double a, double d);
bool isSym(std::string, std::string *);
void transPre(const char * , std::vector<std::string>&  ,std::vector<std::string>& , bool );
std::string calculate(std::vector<std::string>& ,std::vector<double>& );
bool isTwoOp(std::string& );

extern "C" JNIEXPORT jstring JNICALL
//기본적으로 결과창에 띄워 줄 메시지.
Java_com_example_hw_11_MainActivity_stringFromJNI(JNIEnv *env, jobject /* this */)
{
    std::string hello = "This is result";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
//복호화
Java_com_example_hw_11_MainActivity_decryptFromJNI(JNIEnv *env, jobject /* this */, jintArray in)
{
    uint8_t key[] = { 0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c };
    uint8_t iv[]  = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };

    int n = env->GetArrayLength(in);


    uint8_t *out = new uint8_t[n];
    jint *temp = new jint[n];

    struct AES_ctx ctx;

    env->GetIntArrayRegion(in,0,n,temp);
    for(int i = 0; i<n; i++)
    {
        out[i] = temp[i];
    }

    AES_init_ctx_iv(&ctx, key, iv);
    AES_CBC_decrypt_buffer(&ctx,out,n);

    jstring javaString = env->NewStringUTF((const char*)out);
    return javaString;
}


extern "C" JNIEXPORT jintArray JNICALL
//암호화1111
Java_com_example_hw_11_MainActivity_encrypt1FromJNI(JNIEnv *env, jobject /* this */, jintArray in) {

    struct AES_ctx ctx;
    uint8_t key[] = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09,
                     0xcf, 0x4f, 0x3c};
    uint8_t iv[] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c,
                    0x0d, 0x0e, 0x0f};

    int n = env->GetArrayLength(in);
    int *temp;
    uint8_t *out;
    if (n < 64) {
        temp = new int[64];
        out = new uint8_t[64];
    } else {
        temp = new int[n];
        out = new uint8_t[n];
    }
    env->GetIntArrayRegion(in, 0, n, temp);
    for (int i = n; i < 64; i++) {
        temp[i] = 0;
    }

    jintArray tout;
    if (n < 64)
    {
        for(int i = 0; i<64; i++)
        {
            out[i] = (uint8_t)temp[i];
        }
        AES_init_ctx_iv(&ctx, key, iv);
        AES_CBC_encrypt_buffer(&ctx, out,64);
        for(int i = 0; i<64; i++)
        {
            temp[i] = (int)out[i];
        }
        tout = env->NewIntArray(64);
        env->SetIntArrayRegion(tout,0,64,temp);
    } else {
        for(int i = 0; i<n; i++)
        {
            out[i] = (uint8_t)temp[i];
        }
        AES_init_ctx_iv(&ctx, key, iv);
        AES_CBC_encrypt_buffer(&ctx, out,n);
        for(int i = 0; i<n; i++)
        {
            temp[i] = (int)out[i];
        }
        tout = env->NewIntArray(n);
        env->SetIntArrayRegion(tout,0,n,temp);
    }
    return tout;
}


extern "C" JNIEXPORT jintArray JNICALL
//암호화
Java_com_example_hw_11_MainActivity_encryptFromJNI(JNIEnv *env, jobject /* this */, jstring in)
{
    struct AES_ctx ctx;
    uint8_t key[] = { 0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c };
    uint8_t iv[]  = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };
    int n = env->GetStringLength(in);

    const char * temp;
    temp = env->GetStringUTFChars(in,0);
    env->ReleaseStringUTFChars(in,temp);

    AES_init_ctx_iv(&ctx, key, iv);
    AES_CBC_encrypt_buffer(&ctx, (uint8_t*)temp,n);

    int *out = new int[n];
    for(int i = 0 ; i<n; i++)
    {
        out[i] = (uint8_t)temp[i];
    }

    jintArray tout = env->NewIntArray(n);
    env->SetIntArrayRegion(tout,0,n,out);
    return tout;
}

extern "C" JNIEXPORT jstring JNICALL
//실질적으로 계산을 담당하는 함수
Java_com_example_hw_11_MainActivity_calculateFromJNI(JNIEnv *env, jobject, jstring text)
{
    // TODO: implement calculateFromJNI()
    std::vector<std::string> vector_op;
    std::vector<double> vector_num;
    std::vector<std::string> vector_postfix;
    std::vector<std::string> vec;
    double result = 0;
    double ca;
    long long size;
    //- 기호가 연산자인지 음수표시인지 구분해주는 역할.
    bool minus = true;
    std::string result_text = "";
    const char *temp = env->GetStringUTFChars(text, 0);

    //후위 연산식으로 바꿔주는 함수.
    transPre(temp,vector_postfix,vector_op,minus);

    //실제 연산하는 함수.
    result_text = calculate(vector_postfix,vector_num);

    return env->NewStringUTF(result_text.c_str());
}

int priority(char op)
{
    switch(op){
        case '+' :
        case '-' :
            return 1;
        case '*' :
        case '/' :
        case '^' :
            return 2;
        case '_' :
            return 3;
        case '(' :
        case ')' :
            return 4;

        default :
            return 2;
    }
}

bool isSym(std::string str,std::string* temp)
{
    if(str.compare("π") == 0) {
        *temp = "3.14159265359";
        return true;
    }
    if(str.compare("e") == 0) {
        *temp = "2.71828182846";
        return true;
    }

    return false;
}

double operate(double a, std::string op)
{
    a= (a*3.14159265359)/180;
    if(op == "sin"){
        return sin(a);
    } else if(op == "cos"){
        return cos(a);
    } else if(op == "tan"){
        return tan(a);
    }
    return 0;
}

double operate(double a, double b, char op)
{
    switch(op){
        case '+' :
            return a+b;
        case '-' :
        case '_' :
            return a-b;
        case '*' :
            return a*b;
        case '/' :
            return a/b;
        case '^' :
            return squ(a,b);
    }
    return 0;
}

double squ(double a, double ca) {
    if(ca != (int)ca) return pow(a,ca);
    if(ca >= 128) return pow(a,ca);

    double result = 1;
    int b = (int)ca;
    while(b) {
        if(b % 2 == 1) (result *= a);
        a = a * a;
        b = b>>1;
    }
    return result;
}

void transPre(const char * temp, std::vector<std::string>& vector_postfix ,std::vector<std::string>& vector_op, bool minus){
    char* tok1 = strtok((char*)temp," ");
    while(tok1!=NULL){
        //후위연산식으로 바꾸는 과정
        std::string cal_text = tok1;
        std::string t_c;
        int count = 0;
        double t_n = atof(cal_text.c_str());
        if((t_n != 0) || (t_n == 0 && cal_text.at(0) == '0')){
            //입력한 문자가 0을 포함한 수 일경우
            vector_postfix.push_back(cal_text);
            minus = false;
        } else if(isSym(tok1,&t_c)){
            vector_postfix.push_back(t_c);
            minus = false;
        } else{
            if(minus && cal_text.at(0) == '-') {
                vector_postfix.push_back("0");
                //음수 부호를 나타내는 임의지정한 특수문자.
                vector_op.push_back("_");
            } else if(vector_op.empty()) {
                vector_op.push_back(cal_text);
            } else if(cal_text.at(0) == ')'){
                while(vector_op.back().at(0) != '('){
                    vector_postfix.push_back(vector_op.back());
                    vector_op.pop_back();
                }
                vector_op.pop_back();
            } else if(priority(vector_op.back().at(0)) >= priority(cal_text.at(0))){
                if(vector_op.back().at(0) == '(') {
                    vector_op.push_back(cal_text);
                    tok1 = strtok(NULL," ");
                    continue;
                } else {
                    vector_postfix.push_back(vector_op.back());
                    vector_op.pop_back();
                    vector_op.push_back(cal_text);
                }
            } else{
                vector_op.push_back(cal_text);
            }
            minus = false;

            if(cal_text.at(0) == '(') minus = true;
        }
        tok1 = strtok(NULL," ");
    }
    while(!vector_op.empty()) {
        vector_postfix.push_back(vector_op.back());
        vector_op.pop_back();
    }
}

std::string calculate(std::vector<std::string>& vector_postfix,std::vector<double>& vector_num){
    std::string result_text = "";

    for(int i = 0; i<vector_postfix.size(); i++)
    {
        double t_n = atof(vector_postfix.at(i).c_str());
        //숫자일 경우
        if((t_n != 0) || (t_n == 0 && vector_postfix.at(i).at(0) == '0')){
            vector_num.push_back(t_n);
        }
        //연산자일 경우
        else{

            double a,b;
            if(isTwoOp(vector_postfix[i])){
                //2항연산일 경우
                if(vector_num.size() < 2) {
                    result_text = "ER";
                    return result_text;
                }
                b = vector_num.back();
                vector_num.pop_back();
                a = vector_num.back();
                vector_num.pop_back();
                vector_num.push_back(operate(a,b,vector_postfix[i].at(0)));
            } else {
                //1항연산일 경우
                if(vector_num.size() < 1) {
                    result_text = "ER";
                    return result_text;
                }
                b = vector_num.back();
                vector_num.pop_back();
                vector_num.push_back(operate(b,vector_postfix[i]));
            }
        }
    }

    double result = vector_num.front();

    if(result - (long long)result == 0){
        result_text = std::to_string((long long)result);
    } else {
        char * tta = new char[std::to_string(result).length()];
        sprintf(tta,"%g",result);
        result_text = tta;
    }
    return result_text;
}


bool isTwoOp(std::string& str){

    switch(str.at(0)){
        case '+' :
        case '-' :
        case '_' :
        case '*' :
        case '/' :
        case '^' :
            return true;
    }
    return false;
}