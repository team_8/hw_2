package com.example.hw_1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.telephony.mbms.MbmsErrors;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.drawerlayout.widget.DrawerLayout;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private enum Status{
        Bin,Otc,Dec
    }

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
        System.loadLibrary("aes-lib");
    }

    private TextView tv;
    private TextView calcv;
    private int parencount;
    private DrawerLayout lay_draw;
    private View lay_calc;
    private List<String> textarr = new ArrayList<String>();
    private boolean vibrate;
    private boolean error;
    private Vibrator vibrator;
    private SharedPreferences myPrefs;
    private MenuItem m_vibrate;
    private int calcnum;
    private Status status;
    private int b_num[];
    private int b_op[];
    private int b_eng[];
    private int b_pro[];

    private static final String CAPTURE_PATH = "/CAPTURE_TEST";
    private static final String PREFERENCES_NAME = "rebuild_preference";


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        m_vibrate = menu.getItem(3);
        m_vibrate.setChecked(vibrate);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.m_capture:
                capture(lay_calc,false);
                return true;
            case R.id.m_toText:
                sendToText();
                return true;
            case R.id.m_toImage:
                sendToImage();
                return true;
            case R.id.m_vibrate:
                if(item.isChecked()){
                    item.setChecked(false);
                    vibrate = false;
                }else{
                    item.setChecked(true);
                    vibrate = true;
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myPrefs = getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
        vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

        parencount = 0;
        tv = findViewById(R.id.t_result);
        calcv = findViewById(R.id.t_calculate);
        lay_draw = findViewById(R.id.lay_draw);
        lay_calc = findViewById(R.id.lay_calc);
        vibrate = false;
        error = false;

        tv.setText(stringFromJNI());
        calcv.setText("");
        calcv.setMovementMethod(new ScrollingMovementMethod());

//        tv.setText(myPrefs.getString("result",stringFromJNI()));
//        calcv.setText(myPrefs.getString("calc",""));
        parencount = myPrefs.getInt("parencount",0);
        vibrate = myPrefs.getBoolean("vibrate",false);
        error = myPrefs.getBoolean("error",false);
        status = Status.values()[myPrefs.getInt("status",Status.Dec.ordinal())];

        String re = myPrefs.getString("encryptR", null);
        if(error) tv.setText(R.string.text_resulterror);
        else{
            if(re == null) tv.setText(stringFromJNI());
            else tv.setText(getEncrypt(re));
        }


        String ca = myPrefs.getString("encryptC", null);
        if(ca == null)
            calcv.setText("");
        else
            calcv.setText(getEncrypt(ca));


        strToArr();
        calcnum = calcv.getText().length();

        allButtonSetup();
        allButtonEnable(false);
        switch (status)
        {
            case Dec:
                allButtonEnable(true);
                break;
            case Bin:
                ButtonEnable(2);
                break;
            case Otc:
                ButtonEnable(8);
                break;
            default:
                break;
        }
    }

    private String getEncrypt(String json){
        JSONArray a = null;
        int[] tt = null;
        String result = null;
        if(json != null)
        {
            try {
                a = new JSONArray(json);
                tt = new int[a.length()];
                Log.d("ln",Integer.toString( a.length()));
                for (int i = 0; i < a.length(); i++) {
                    tt[i] = Integer.parseInt(a.optString(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            result = decryptFromJNI(tt);
        }

        return result;
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = myPrefs.edit();

//        editor.putString("result",tv.getText().toString()); // key, value를 이용하여 저장하는 형태
//        editor.putString("calc",calcv.getText().toString());
        editor.putBoolean("vibrate",vibrate);
        editor.putInt("parencount",parencount);
        editor.putBoolean("error",error);
        editor.putInt("status",status.ordinal());

        JSONArray list;

        String re = tv.getText().toString();
        list = setEncrypt(re);
        editor.putString("encryptR", list.toString());


        String ca = calcv.getText().toString();
        list = setEncrypt(ca);
        editor.putString("encryptC", list.toString());

        editor.commit();
    }

    private JSONArray setEncrypt(String in){
        JSONArray list = new JSONArray();
        int[] a;
        int[] tt = new int[in.length()];
        for(int i = 0; i<in.length(); i++)
        {
            tt[i] = (int)in.charAt(i);
        }
        a = encrypt1FromJNI(tt);
        for(int i= 0; i<a.length; i++)
        {
            list.put(Integer.toString(a[i]));
        }

        return list;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        String re = tv.getText().toString();
        String ca = calcv.getText().toString();

        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.t_result);
        calcv = findViewById(R.id.t_calculate);
        lay_draw = findViewById(R.id.lay_draw);
        lay_calc = findViewById(R.id.lay_calc);

        tv.setText(re);
        calcv.setText(ca);
    }

    private CharSequence arrToStr(){
        String temp = "";

        if(textarr.size() != 0) temp += textarr.get(0);
        for(int i = 1; i<textarr.size(); i++)
        {
            temp += " " + textarr.get(i);
        }
        return temp;
    }

    private void strToArr(){
        String temp = calcv.getText().toString();
        if(temp.equals("")) return;

        String[] array = temp.split(" ");

        for(int i = 0; i<array.length; i++)
        {
            textarr.add(array[i]);
        }
    }

    private boolean lastIsOp(){
        String temp = textarr.get(textarr.size()-1);
        switch (temp)
        {
            case "+" :
            case "-" :
            case "*" :
            case "/" :
            case "^" :
            case "sin" :
            case "cos" :
            case "tan" :
                return true;

            default:
                if(temp.charAt(temp.length()-1) == '.') return true;
                return false;
        }
    }

    private boolean lastIsNum(boolean dot){
        String temp = textarr.get(textarr.size()-1);
        char a = temp.charAt(temp.length()-1);
        if('0' <= a && a <= '9'){
            return true;
        }
        if(dot){
            if(a == '.') return true;
        }
        return false;
    }

    private boolean lastIsSym() {
        String temp = textarr.get(textarr.size()-1);
        switch (temp)
        {
            case "π" :
            case "e" :
                return true;

            default:
                return false;
        }
    }

    private String lastVal(){
        return textarr.get(textarr.size()-1);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("ResourceType")
    //연산기호를 입력했을때 호출되는 함수.
    public void clickChar(View view) {
        allClickEvent();
        //연산자가 2번이상 연속될 경우 또는 소수점 이후 바로 연산자를 입력할 경우 입력되지 않게함.
        if(textarr.isEmpty() || lastIsOp() || lastVal().charAt(0) == '(') return;
        Button b = (Button)view;
        textarr.add(b.getText().toString());

        calcv.setText(arrToStr());
    }

    public void clickMinus(View view) {
        allClickEvent();
        //연산자가 2번이상 연속될 경우 또는 소수점 이후 바로 연산자를 입력할 경우 입력되지 않게함.
        Button b = (Button)view;
        if(calcnum + b.getText().length()+2 > 64) return;
        if(textarr.isEmpty() || !lastIsOp()){
            textarr.add(b.getText().toString());
        } else {
            return;
        }
        calcv.setText(arrToStr());
        calcnum = calcv.getText().length();
    }
    //숫자를 입력했을 때 호출되는 함수
    public void clickNum(View view) {
        allClickEvent();
        Button b = (Button)view;
        if(calcnum + b.getText().length()+2 > 64) return;
        if(textarr.isEmpty()){
            textarr.add(b.getText().toString());
        } else if(lastIsNum(true)){
            String val = lastVal();
            val += b.getText();
            textarr.set(textarr.size()-1,val);
        } else{
            // ')' ,기호 다음에는 숫자를 입력할 수 없음.
            if(lastVal().charAt(0) == ')') return;
            if(lastIsSym()) return;
            textarr.add(b.getText().toString());
        }

        calcv.setText(arrToStr());
        calcnum = calcv.getText().length();
    }
    //소수점을 입력했을 때 호출되는 함수
    public void clickDot(View view) {
        allClickEvent();
        Button b = (Button)view;
        if(calcnum + b.getText().length()+2 > 64) return;
        if(textarr.isEmpty()) {
            textarr.add(b.getText().toString());
        } else if(lastIsNum(true)) {
            String val = lastVal();
            //소수점을 입력하려는데 마지막 문자열이 숫자이고 소수점이 이미 찍혀있다면 입력하지 않음
            for(int i = 0; i<val.length();i++){
                if(val.charAt(i) == '.'){
                    return;
                }
            }
            val += b.getText();
            textarr.remove(textarr.size()-1);
            textarr.add(val);
        } else if(lastVal().charAt(0) == ')') {
            return;
        } else {
            textarr.add(b.getText().toString());
        }
        calcv.setText(arrToStr());
        calcnum = calcv.getText().length();
    }

    public void clickLparen(View view) {
        allClickEvent();
        Button b = (Button)view;
        if(calcnum + b.getText().length()+2 > 64) return;
        //여는 괄호 입력전에는 아무것도 없거나 연산자 이거나 ( 여야함.
        if(textarr.isEmpty() || lastVal().charAt(0) == '('){
            textarr.add(b.getText().toString());
            parencount++;
        } else if(lastIsOp()){
            //마지막 문자가 .인경우 입력받지 않음.
            if(lastIsNum(true)) return;
            textarr.add(b.getText().toString());
            parencount++;
        }
        else {
            return;
        }
        calcv.setText(arrToStr());
        calcnum = calcv.getText().length();
    }

    public void clickRparen(View view) {
        allClickEvent();
        Button b = (Button)view;
        if(calcnum + b.getText().length()+2 > 64) return;
        if(textarr.isEmpty()) return;
        //괄호의 짝이 안맞으면 입력받지 않음.
        if(parencount == 0) return;
        //닫는 괄호 입력 전에 연산자가 있으면 안됨.
        if(lastVal().charAt(0) == '(' || lastIsOp()) return;
        parencount--;
        textarr.add(b.getText().toString());

        calcv.setText(arrToStr());
        calcnum = calcv.getText().length();
    }

    //캔슬 버튼을 클릭했을 때 호출되는 함수
    public void clickCancel(View view) {
        // Do something in response to button
        allClickEvent();
        allClear();
    }

    //백 버튼을 클릭했을 때 호출되는 함수
    public void clickBack(View view) {
        allClickEvent();
        if(textarr.isEmpty()) return;
        String a;
        if(lastIsNum(true)){
            a = lastVal();
            if(a.length() == 1) textarr.remove(textarr.size()-1);
            else{
                a = a.substring(0,a.length()-1);
                textarr.set(textarr.size()-1,a);
            }
        }else{
            if(lastVal().charAt(0) == '(') parencount--;
            if(lastVal().charAt(0) == ')') parencount++;
            textarr.remove(textarr.size()-1);
        }

        calcv.setText(arrToStr());
        calcnum = calcv.getText().length();
    }

    public void clickTriFun(View view) {
        allClickEvent();
        Button b = (Button)view;
        if(calcnum + b.getText().length()+2 > 64) return;
        if(!textarr.isEmpty()){
            if(lastIsNum(true)) return;
            if(lastIsSym()) return;
        }
        textarr.add(b.getText().toString());

        textarr.add("(");
        parencount++;

        calcv.setText(arrToStr());
        calcnum = calcv.getText().length();
    }

    public void clickSymbol(View view) {
        allClickEvent();
        Button b = (Button)view;
        if(calcnum + b.getText().length()+2 > 64) return;
        if(!textarr.isEmpty()){
            if(lastIsNum(true)) return;
            if(lastIsSym()) return;
        }
        textarr.add(b.getText().toString());

        calcv.setText(arrToStr());
        calcnum = calcv.getText().length();
    }


    //등호 버튼을 클릭 했을 때 호출되는 함수
    public void clickResult(View view) {
        allClickEvent();
        String temp;
        if(parencount != 0) {
            temp = getString(R.string.text_resulterror);
            error = true;
        } else {
            temp = calcv.getText().toString();
            error = false;
        }
        if(temp.equals("")) return;
        temp = calculateFromJNI(temp);
        if(temp.equals("ER")) {
            temp = getString(R.string.text_resulterror);
            error = true;
        }

        tv.setText(temp);
    }


    public void clickBin(View view) {
        allClickEvent();
        if(status == Status.Bin) return;
        status = Status.Bin;
        allButtonEnable(false);
        ButtonEnable(2);
        allClear();
    }
    public void clickOtc(View view) {
        allClickEvent();
        if(status == Status.Bin) return;
        status = Status.Otc;
        allButtonEnable(false);
        ButtonEnable(8);
        allClear();
    }

    public void clickDec(View view) {
        allClickEvent();
        if(status == Status.Dec) return;
        status = Status.Dec;
        allButtonEnable(true);
        allClear();
    }

    private void ButtonEnable(int n) {
        for(int i = 0; i<n; i++)
        {
            findViewById(b_num[i]).setEnabled(true);
        }
        for(int i = 0; i<b_op.length; i++)
        {
            findViewById(b_op[i]).setEnabled(true);
        }
        for(int i = 0; i<b_pro.length; i++)
        {
            findViewById(b_pro[i]).setEnabled(true);
        }
    }

    private void allButtonEnable(boolean b) {
        for(int i = 0; i<b_num.length; i++)
        {
            findViewById(b_num[i]).setEnabled(b);
        }
        for(int i = 0; i<b_op.length; i++)
        {
            findViewById(b_op[i]).setEnabled(b);
        }
        for(int i = 0; i<b_eng.length; i++)
        {
            findViewById(b_eng[i]).setEnabled(b);
        }
        for(int i = 0; i<b_pro.length; i++)
        {
            findViewById(b_pro[i]).setEnabled(b);
        }
    }

    private void allButtonSetup() {
        String t_op[] = {"plus","minus","mul","div","back","cancel","result"};
        String t_eng[] = {"squared","pi","e","sin","cos","tan","dot","Lparen","Rparen"};
        String t_pro[] = {"dec","bin","otc","hex"};
        b_num = new int[10];
        b_op = new int[t_op.length];
        b_eng = new int[t_eng.length];
        b_pro = new int[t_pro.length];
        for (int i=0; i < 10; i++) {
            String resName = "@id/b_" + i;
            b_num[i] = getResources().getIdentifier(resName, "id", "com.example.hw_1");
        }
        for (int i=0; i < b_op.length; i++) {
            String resName = "@id/b_" + t_op[i];
            b_op[i] = getResources().getIdentifier(resName, "id", "com.example.hw_1");
        }
        for (int i=0; i < b_eng.length; i++) {
            String resName = "@id/b_" + t_eng[i];
            b_eng[i] = getResources().getIdentifier(resName, "id", "com.example.hw_1");
        }
        for (int i=0; i < b_pro.length; i++) {
            String resName = "@id/b_" + t_pro[i];
            b_pro[i] = getResources().getIdentifier(resName, "id", "com.example.hw_1");
        }

    }

    public void clickLside(View view) {
        lay_draw.openDrawer(Gravity.LEFT);
    }
    public void clickRside(View view) {
        lay_draw.openDrawer(Gravity.RIGHT);
    }



    private void sendToImage() {
        capture(lay_calc,true);
    }

    private void sendToText() {
        String text = "";
        text += calcv.getText() + " =\n" + tv.getText();
        try{
            Intent textIntent = new Intent(Intent.ACTION_SEND);
            textIntent.setType("text/plain");
            textIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(textIntent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void capture(View view,boolean send) {
        File dir = null;
        String sdcard = Environment.getExternalStorageState();

        if ( !sdcard.equals(Environment.MEDIA_MOUNTED)) {
            // SD카드가 마운트되어있지 않음
            dir = Environment.getRootDirectory();
            Log.d("마운트","NO");
        } else {
            // SD카드가 마운트되어있음
            dir = Environment.getExternalStorageDirectory();
            Log.d("마운트","YES");
        }

        String strFolderPath = dir.getAbsolutePath() + CAPTURE_PATH;
        File folder = new File(strFolderPath);
        if(!folder.exists()) {
            folder.mkdirs();
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date currentTime_1 = new Date();
        String dateString = formatter.format(currentTime_1);

        String strFilePath = strFolderPath + "/" + dateString + ".png";

        View mView = view;

        Bitmap screenBMP = Bitmap.createBitmap(mView.getMeasuredWidth(),
                mView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas screenShotCanvas = new Canvas(screenBMP);
        mView.draw(screenShotCanvas);


        if(send){
            File file = new File(strFilePath);
            try {
                FileOutputStream fos = new FileOutputStream(file);
                screenBMP.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
                sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + strFilePath)));

                Uri uri = FileProvider.getUriForFile(this, "com.example.hw_1.fileprovider", file);

                try{
                    Intent ImageIntent = new Intent(Intent.ACTION_SEND);
                    ImageIntent.putExtra(Intent.EXTRA_STREAM, uri);

                    ImageIntent.setType("image/*");

                    startActivity(ImageIntent);
                }catch (Exception e){
                    e.printStackTrace();
                }

            } catch (FileNotFoundException e) {
                Log.d("error",e.getMessage());
            } catch (IOException e) {
                Log.d("error",e.getMessage());
            } catch (Exception e) {
                Log.d("error",e.getMessage());
            }

        } else {
            try {
                FileOutputStream fos = new FileOutputStream(strFilePath);
                screenBMP.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();

            } catch (FileNotFoundException e) {
                Log.d("error",e.getMessage());
            } catch (IOException e) {
                Log.d("error",e.getMessage());
            } catch (Exception e) {
                Log.d("error",e.getMessage());
            }
            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
                // 읽기 쓰기 모두 가능
                Toast.makeText(getApplicationContext(), getString(R.string.text_capture) + "\n" + strFilePath, Toast.LENGTH_LONG).show();
            }

            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + strFilePath)));
        }
    }


    private void allClickEvent() {
        if(vibrate){
            vibrator.vibrate(40);
        }
    }

    private void allClear() {
        parencount = 0;
        calcv.setText("");
        textarr.clear();
        tv.setText("This is result");
        calcnum = calcv.getText().length();
    }



    public native String stringFromJNI();
    public native String calculateFromJNI(String text);
    public native int[] encrypt1FromJNI(int[] in);
    public native int[] encryptFromJNI(String in);
    public native String decryptFromJNI(int[] in);
}


